package com.project.base.user.user.service;

import com.project.base.user.user.domain.UserDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface UserService {
    Flux<UserDto> getAllUsers();

    Mono<UserDto> createUser(UserDto userDto);

    Mono<UserDto> getUser(String userId);

    Mono<UserDto> updateUser(String userId, UserDto userDto);

    Mono<Void> deleteUser(String userId);
}
