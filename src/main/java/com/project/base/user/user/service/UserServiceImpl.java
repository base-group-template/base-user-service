package com.project.base.user.user.service;

import com.project.base.user.user.component.UserComponent;
import com.project.base.user.user.domain.UserDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class UserServiceImpl implements UserService {

    private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
    private final UserComponent userComponent;

    public UserServiceImpl(UserComponent userComponent) {
        this.userComponent = userComponent;
    }

    @Override
    public Flux<UserDto> getAllUsers() {
        return userComponent.findAll();
    }

    @Override
    public Mono<UserDto> createUser(UserDto userDto) {
        return userComponent.save(userDto);
    }

    @Override
    public Mono<UserDto> getUser(String userId) {
        return userComponent.find(userId);
    }

    @Override
    public Mono<UserDto> updateUser(String userId, UserDto userDto) {
        return userComponent.update(userId, userDto);
    }

    @Override
    public Mono<Void> deleteUser(String userId) {
        return userComponent.delete(userId);
    }
}
