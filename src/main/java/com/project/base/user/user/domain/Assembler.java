package com.project.base.user.user.domain;

import com.project.base.user.user.entity.User;

public class Assembler {

    public static User toEntity(UserDto source) {
        return new User(
            source.getId(),
            source.getFirstName(),
            source.getLastName()
        );
    }

    public static UserDto toDomain(User source) {
        return new UserDto(
            source.getId(),
            source.getFirstName(),
            source.getLastName()
        );
    }
}
