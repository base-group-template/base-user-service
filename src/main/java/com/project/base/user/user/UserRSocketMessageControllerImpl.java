package com.project.base.user.user;

import com.project.base.user.identity.domain.UserRepresentationDto;
import com.project.base.user.identity.service.IdentityService;
import com.project.base.user.user.domain.UserDto;
import com.project.base.user.user.domain.requests.CreateUser;
import com.project.base.user.user.domain.requests.DeleteUser;
import com.project.base.user.user.domain.requests.GetUser;
import com.project.base.user.user.domain.requests.UpdateUser;
import com.project.base.user.user.service.UserService;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

@Controller
@PreAuthorize("isAuthenticated()")
public class UserRSocketMessageControllerImpl implements UserRSocketMessageController {

    private final UserService userService;
    private final IdentityService identityService;

    public UserRSocketMessageControllerImpl(UserService userService, IdentityService identityService) {
        this.userService = userService;
        this.identityService = identityService;
    }

    @MessageMapping("user.getAllUsers")
    //TODO: the creation of roles for users is pending. once ready the roles can be validated
    //@PreAuthorize("hasAnyRole('ROLE_CUSTOMER','ROLE_OFFICEADMIN','ROLE_EMPLOYEE')")
    public Flux<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @MessageMapping("user.createUser")
    public Mono<UserDto> createUser(CreateUser request) {
        return userService.createUser(request.getUserDto());
    }

    @MessageMapping("user.getUser")
    public Mono<UserDto> getUser(@Headers Map<String, Object> metadata, GetUser request) {
        return userService.getUser(request.getUserId());
    }

    @MessageMapping("user.updateUser")
    public Mono<UserDto> updateUser(UpdateUser request) {
        return userService.updateUser(request.getUserId(), request.getUserDto());
    }

    @MessageMapping("user.deleteUser")
    public Mono<Void> deleteUser(DeleteUser request) {
        return userService.deleteUser(request.getUserId());
    }

    @MessageMapping("user.identityTest")
    public Flux<UserRepresentationDto> identityTest() {
        return identityService.getAllUsers();
    }
}
