package com.project.base.user.user.component;

import com.project.base.user.user.domain.UserDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

public interface UserComponent {
    Flux<UserDto> findAll();

    Mono<UserDto> save(UserDto user);

    Mono<UserDto> find(String userId);

    Mono<UserDto> update(String userId, UserDto user);

    Mono<Void> delete(String userId);

}
