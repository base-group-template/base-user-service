package com.project.base.user.user;

import com.project.base.user.user.domain.UserDto;
import com.project.base.user.user.domain.requests.CreateUser;
import com.project.base.user.user.domain.requests.DeleteUser;
import com.project.base.user.user.domain.requests.GetUser;
import com.project.base.user.user.domain.requests.UpdateUser;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;

interface UserRSocketMessageController {

    Flux<UserDto> getAllUsers();

    Mono<UserDto> createUser(CreateUser request);

    Mono<UserDto> getUser(Map<String, Object> metadata, GetUser request);

    Mono<UserDto> updateUser(UpdateUser request);

    Mono<Void> deleteUser(DeleteUser request);
}
