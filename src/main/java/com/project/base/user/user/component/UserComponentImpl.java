package com.project.base.user.user.component;

import com.project.base.user.user.domain.Assembler;
import com.project.base.user.user.domain.UserDto;
import com.project.base.user.user.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Component
public class UserComponentImpl implements UserComponent {
    private static final Logger logger = LoggerFactory.getLogger(UserComponentImpl.class);

    private final UserRepository userRepository;

    public UserComponentImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public Flux<UserDto> findAll() {
        return userRepository.findAll()
            .map(Assembler::toDomain);
    }

    @Override
    public Mono<UserDto> save(UserDto user) {
        return userRepository.save(Assembler.toEntity(user))
            .map(Assembler::toDomain);
    }

    @Override
    public Mono<UserDto> find(String userId) {
        return userRepository.findById(userId)
            .map(Assembler::toDomain)
            .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<UserDto> update(String userId, UserDto userDto) {
        return find(userId)
            .flatMap(user -> {
                    userDto.setId(userId);
                    return save(userDto);
                }
            )
            .switchIfEmpty(Mono.empty());
    }

    @Override
    public Mono<Void> delete(String userId) {
        return userRepository.deleteById(userId);
    }

}
