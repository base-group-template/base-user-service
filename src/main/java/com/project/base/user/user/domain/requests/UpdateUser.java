package com.project.base.user.user.domain.requests;

import com.project.base.user.user.domain.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UpdateUser {
    private String userId;
    private UserDto userDto;
}
