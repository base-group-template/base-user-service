package com.project.base.user.user.domain.requests;

import com.project.base.user.user.domain.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CreateUser {
    private UserDto userDto;
}
