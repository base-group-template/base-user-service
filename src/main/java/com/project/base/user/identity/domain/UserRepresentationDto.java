package com.project.base.user.identity.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.Map;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRepresentationDto {
    private Boolean enabled;
    private Boolean emailVerified;
    private String email;
    private String firstName;
    private String lastName;
    private List<CredentialRepresentationDto> credentials;
    private Map<String, List<String>> attributes;
    private Long createdTimestamp;
}
