package com.project.base.user.identity.configuration;

import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.keycloak.OAuth2Constants.PASSWORD;

@Configuration
public class IdentityServiceConfiguration {

    @Value("${identity-service.auth-server-url}")
    private String authUrl;
    @Value("${identity-service.realm}")
    private String realm;
    @Value("${identity-service.credentials.client-id}")
    private String clientId;
    @Value("${identity-service.credentials.secret}")
    private String secretKey;
    @Value("${identity-service.credentials.username}")
    private String username;
    @Value("${identity-service.credentials.password}")
    private String password;

    @Bean
    public RealmResource realmResource() {
        return KeycloakBuilder.builder()
            .grantType(PASSWORD)
            .serverUrl(authUrl)
            .realm(realm)
            .clientId(clientId)
            .clientSecret(secretKey)
            .username(username)
            .password(password)
            .build()
            .realm(realm);
    }
}
