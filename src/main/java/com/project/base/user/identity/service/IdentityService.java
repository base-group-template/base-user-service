package com.project.base.user.identity.service;

import com.project.base.user.identity.domain.UserRepresentationDto;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface IdentityService {
    Mono<UserRepresentationDto> createUser(UserRepresentationDto userRepresentationDto);

    Mono<UserRepresentationDto> getUser(String userId);

    Mono<UserRepresentationDto> updateUser(UserRepresentationDto userRepresentationDto);

    Flux<UserRepresentationDto> getAllUsers();

    Mono<Void> deleteUser(String userId);
}