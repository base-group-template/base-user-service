package com.project.base.user.identity.service;

import com.project.base.user.identity.domain.Assembler;
import com.project.base.user.identity.domain.UserRepresentationDto;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
public class IdentityServiceImpl implements IdentityService {

    private final RealmResource realmResource;

    private final Assembler da = new Assembler();

    private static final Logger logger = LoggerFactory.getLogger(IdentityServiceImpl.class);

    public IdentityServiceImpl(RealmResource realmResource) {
        this.realmResource = realmResource;
    }

    @Override
    public Mono<UserRepresentationDto> createUser(UserRepresentationDto userRepresentationDto) {
        return null;
    }

    @Override
    public Mono<UserRepresentationDto> getUser(String userId) {
        UserRepresentation user = realmResource.users().get(userId).toRepresentation();

        if (user != null) {
            return Mono.just(user).map(da::toDomain);
        }

        return Mono.empty();
    }

    @Override
    public Mono<UserRepresentationDto> updateUser(UserRepresentationDto userRepresentationDto) {
        return null;
    }

    @Override
    public Flux<UserRepresentationDto> getAllUsers() {
        return Flux.fromStream(
            users().stream().map(da::toDomain)
        );
    }

    @Override
    public Mono<Void> deleteUser(String userId) {
        return null;
    }

    private List<UserRepresentation> users() {
        return realmResource.users().list();
    }
}
