package com.project.base.user.identity.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CredentialRepresentationDto {
    private String type;
    private Boolean temporary;
    private String value;
}
