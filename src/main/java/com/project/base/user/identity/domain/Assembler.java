package com.project.base.user.identity.domain;

import org.keycloak.representations.idm.CredentialRepresentation;
import org.keycloak.representations.idm.UserRepresentation;

import java.util.stream.Collectors;

public class Assembler {

    public CredentialRepresentation toEntity(CredentialRepresentationDto source) {
        CredentialRepresentation credentialRepresentation = new CredentialRepresentation();
        credentialRepresentation.setType(source.getType());
        credentialRepresentation.setTemporary(source.getTemporary());
        credentialRepresentation.setValue(source.getValue());

        return credentialRepresentation;
    }

    public CredentialRepresentationDto toDomain(CredentialRepresentation source) {
        return new CredentialRepresentationDto(
            source.getType(),
            source.isTemporary(),
            source.getValue()
        );
    }

    public UserRepresentation toEntity(UserRepresentationDto source) {
        UserRepresentation userRepresentation = new UserRepresentation();
        userRepresentation.setEnabled(source.getEnabled());
        userRepresentation.setEmailVerified(source.getEmailVerified());
        userRepresentation.setEmail(source.getEmail());
        userRepresentation.setFirstName(source.getFirstName());
        userRepresentation.setLastName(source.getLastName());
        userRepresentation.setCredentials(source.getCredentials().stream().map(this::toEntity).collect(Collectors.toList()));
        userRepresentation.setAttributes(source.getAttributes());
        userRepresentation.setCreatedTimestamp(source.getCreatedTimestamp());

        return userRepresentation;
    }

    public UserRepresentationDto toDomain(UserRepresentation source) {
        return new UserRepresentationDto(
            source.isEnabled(),
            source.isEmailVerified(),
            source.getEmail(),
            source.getFirstName(),
            source.getLastName(),
            null,
            source.getAttributes(),
            source.getCreatedTimestamp()
        );
    }
}
