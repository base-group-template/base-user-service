package com.project.base.user.root;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ServerWebExchange;

import java.net.URI;

@RestController
public class RootRestController {

    @GetMapping
    ResponseEntity<Void> getBasicInformation(ServerWebExchange webExchange) {
        ServerHttpRequest request = webExchange.getRequest();

        return ResponseEntity
            .status(HttpStatus.FOUND)
            .location(
                URI.create(request.getURI() + "actuator/info")
            ).build();
    }

}
