package com.project.base.user.core.configuration.rsocket;

import io.rsocket.metadata.WellKnownMimeType;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.codec.StringDecoder;
import org.springframework.http.codec.cbor.Jackson2CborDecoder;
import org.springframework.http.codec.cbor.Jackson2CborEncoder;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.http.codec.json.Jackson2JsonEncoder;
import org.springframework.messaging.rsocket.RSocketStrategies;
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler;
import org.springframework.util.MimeType;
import org.springframework.util.MimeTypeUtils;

import java.util.List;

@Configuration
public class RSocketConfiguration {

    private final MimeType MESSAGE_RSOCKET_AUTHENTICATION = MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.toString());
    private final MimeType TEXT_PLAIN = MimeTypeUtils.parseMimeType(WellKnownMimeType.TEXT_PLAIN.toString());

    @Bean
    public RSocketMessageHandler rsocketMessageHandler() {
        RSocketMessageHandler handler = new RSocketMessageHandler();
        handler.setRSocketStrategies(rsocketStrategies());
        return handler;
    }

    @Bean
    public RSocketStrategies rsocketStrategies() {
        return RSocketStrategies.builder()
            .metadataExtractorRegistry(registry ->
                registry.metadataToExtract(TEXT_PLAIN, String.class, "token")
            )
            .encoders(encoders ->
                encoders.addAll(
                    List.of(
                        new Jackson2CborEncoder(),
                        new Jackson2JsonEncoder()
                    )
                )
            )
            .decoders(decoders ->
                decoders.addAll(
                    List.of(
                        new Jackson2CborDecoder(),
                        new Jackson2JsonDecoder(),
                        StringDecoder.allMimeTypes()
                    )
                )
            )
            .build();
    }
}
