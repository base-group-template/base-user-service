package com.project.base.user.core.configuration.exception;

import com.project.base.user.user.exception.UserNotFoundException;
import org.springframework.boot.web.error.ErrorAttributeOptions;
import org.springframework.boot.web.reactive.error.DefaultErrorAttributes;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;

import java.util.Map;

@Component
public class GlobalErrorAttributes extends DefaultErrorAttributes {

    private final Map<String, HttpStatus> exceptionMapper = Map.of(
        CustomGenericException.class.getName(), HttpStatus.I_AM_A_TEAPOT,
        UserNotFoundException.class.getName(), HttpStatus.NOT_FOUND
    );

    private HttpStatus status = HttpStatus.BAD_REQUEST;

    @Override
    public Map<String, Object> getErrorAttributes(ServerRequest request, ErrorAttributeOptions options) {
        Map<String, Object> map = super.getErrorAttributes(request, options);
        map.put("status", getStatus(map.get("exception").toString()));
        return map;
    }

    public HttpStatus getStatus(String exception) {
        if (exceptionMapper.containsKey(exception)) {
            return exceptionMapper.get(exception);
        }

        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }
}
