package com.project.base.user.core.configuration.exception;

public class CustomGenericException extends Exception {
    public CustomGenericException() {
    }

    public CustomGenericException(String message) {
        super(message);
    }
}
