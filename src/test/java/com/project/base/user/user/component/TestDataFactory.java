package com.project.base.user.user.component;

import com.project.base.user.user.entity.User;

import java.util.Arrays;
import java.util.List;

public class TestDataFactory {

    public List<User> getUsers() {
        User user1 = new User(
            "0",
            "Vito",
            "Corleone"
        );
        User user2 = new User(
            "1",
            "Max",
            "Mad"
        );

        return Arrays.asList(
            user1,
            user2
        );
    }
}
