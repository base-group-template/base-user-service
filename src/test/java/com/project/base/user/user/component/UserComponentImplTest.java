package com.project.base.user.user.component;

import com.project.base.user.user.domain.UserDto;
import com.project.base.user.user.repository.UserRepository;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;

@Disabled("Example on how to use a testing profile with DB connection. Spring Boot excludes Connecting to a DB during Maven build.")
@DataMongoTest
@Import(UserComponentImpl.class)
@ActiveProfiles("test")
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class UserComponentImplTest {

    private final UserComponentImpl userComponent;
    private final UserRepository userRepository;

    private final TestDataFactory testDataFactory;

    public UserComponentImplTest(
        @Autowired UserComponentImpl userComponent,
        @Autowired UserRepository userRepository
    ) {
        this.userComponent = userComponent;
        this.userRepository = userRepository;

        testDataFactory = new TestDataFactory();
    }

    @BeforeAll
    void createTestData() {
        userRepository.saveAll(testDataFactory.getUsers()).collectList().block();
    }

    @AfterAll
    void deleteTestData() {
        userRepository.deleteAll().block();
    }

    @Test
    void getAllUsers() {
        StepVerifier
            .create(userComponent.findAll())
            .recordWith(ArrayList::new)
            .expectNextCount(2)
            .consumeRecordedWith(users -> {
                    assertThat(users.stream().findFirst().orElse(null)).isInstanceOf(UserDto.class);
                    assertThat(users.stream().map(UserDto::getId).collect(Collectors.toList())).containsExactlyInAnyOrder("0", "1");
                }
            )
            .verifyComplete();
    }

    @Test
    void createUser() {
    }

    @Test
    void getUser() {
        String userId = "0";

        StepVerifier
            .create(userComponent.find(userId))
            .assertNext(user -> {
                assertThat(user).isNotNull();
                assertThat(user).isInstanceOf(UserDto.class);
                assertThat(user.getId()).isEqualTo(userId);
                assertThat(user.getLastName()).isEqualTo("Corleone");
            })
            .verifyComplete();
    }

    @Test
    void getUser_userNotFound() {
        String userId = "-1";

        StepVerifier
            .create(userComponent.find(userId))
            .verifyComplete();
    }

    @Test
    void updateUser() {
        String userId = "1";
        String newLastName = "Insane";
        UserDto userToUpdate = userComponent.find(userId).block();
        Objects.requireNonNull(userToUpdate).setLastName(newLastName);

        StepVerifier
            .create(userComponent.update(userId, userToUpdate))
            .assertNext(user -> {
                assertThat(user).isNotNull();
                assertThat(user).isInstanceOf(UserDto.class);
                assertThat(user.getId()).isEqualTo(userId);
                assertThat(user.getLastName()).isEqualTo(newLastName);
            })
            .verifyComplete();
    }

    @Test
    void updateUser_userNotFound() {
        String userId = "-1";

        StepVerifier
            .create(userComponent.update(userId, mock(UserDto.class)))
            .verifyComplete();
    }

    @Test
    void deleteUser() {
    }
}